# Other groups you may encounter 
## Pinewood Emergency Team (PET)
The Pinewood Emergency Team is set up as an emergency response group to a few types of emergencies at Pinewood facilities. They often help PBST in our mission to keep them safe. If no PET members are on-site and a fire or radiation spill needs to be dealt with at once, PBST is allowed to help put out the fire or stop the spill.

PET has 3 subdivisions: Medical, Fire & Hazmat, each with their own uniform. The Fire & Hazmat uniforms allow you to wear a PBST uniform underneath it, the Medical uniform doesn’t (see the Uniform chapter).

You can switch between being on-duty for PBST or PET by using the !setgroup command. PET has its own set of tools, and you may not use PET tools when on-duty for PBST, and vice versa.

Read the PET handbook for more details on how PET works: 
 - [Pinewood Emergency Team - Handbook (Outdated)](https://devforum.roblox.com/t/pinewood-emergency-team-handbook/507807)
 - [Pinewood Emergency Team - Website (More Outdated)](https://pet.pinewood-builders.com/)

The Mayhem Syndicate is the opposite of PBST. Where we intend to protect the core, they intend to melt or freeze it. They often join a game in a raid, and when this happens you are advised to call for backup.

TMS can be recognized by the red ranktag and the black uniform. Any TMS with ranktag+uniform or only ranktag and still actively trying to destroy the core is Kill on Sight (KoS) by default. The only place where TMS may not be killed is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

## The Mayhem Syndicate (TMS)
The Mayhem Syndicate is the opposite of PBST. Where we intend to protect the core, they intend to melt or freeze it. They often join a game in a raid, and when this happens you are advised to call for backup.

TMS can be recognized by the red ranktag and the black uniform. Any TMS with ranktag+uniform or only ranktag and still actively trying to destroy the core is Kill on Sight (KoS) by default. The only place where TMS may not be killed is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

![img](/tms_loadout.jpeg)
